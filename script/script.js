let clothes = {
type : 'dress',
size : {
    width: 35,
    height : 55,
    weight: {
        s: {
            xs: 200,
            xxs: 150
        },
        m: 300,
        l: 500,
        xl : 800,
    }},
color : {
    white : 5,
    red: 8}
}
function makeClone(obj){
    let clone = {};
    for (let key in obj){
        if(typeof(obj[key]) !== 'object'){
            clone[key] = obj[key];
        }
        else clone[key] = makeClone(obj[key])
    } return clone; 
}

let newClothes = makeClone(clothes);
console.log(newClothes);
clothes.size.width = 40;
clothes.color.white = 7
console.log(clothes);
console.log(newClothes);
newClothes.size.weight.s.xs = 175
console.log(newClothes);
console.log(clothes);
